<?php
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

  $messbirths = array();

  if (!empty($_COOKIE['save'])) {
    setcookie('save', '', 100000);
    $messbirths[] = 'Спасибо, результаты отправлены.';
  }
  if (!empty($_COOKIE['notsave'])) {
    setcookie('notsave', '', 100000);
    $messbirths[] = 'Ошибка отправления.';
  }

  $errors = array();
  $errors['name'] = empty($_COOKIE['name_error']) ? '' : $_COOKIE['name_error'];
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['sverh'] = !empty($_COOKIE['sverh_error']);
  $errors['bio'] = !empty($_COOKIE['bio_error']);
  $errors['consent'] = !empty($_COOKIE['consent_error']);

  // name error print
  if ($errors['name'] == 'null') {
    setcookie('name_error', '', 100000);
    $messages[] = '<div>Заполните имя.</div>';
  }
  else if ($errors['name'] == 'incorrect') {
      setcookie('name_error', '', 100000);
      $messages[] = '<div>Недопустимые символы. Введите имя заново.</div>';
  }

  // email error print
  if ($errors['email']) {
    setcookie('email_error', '', 100000);
    $messages[] = '<div>Заполните почту.</div>';
  }

  // sverh error print
  if ($errors['sverh']) {
    setcookie('sverh_error', '', 100000);
    $messages[] = '<div>Выберите хотя бы одну сверхспособность.</div>';
  }

  if ($errors['bio']) {
    setcookie('bio_error', '', 100000);
    $messages[] = '<div>Напишите что-нибудь о себе.</div>';
  }

  if ($errors['consent']) {
    setcookie('consent_error', '', 100000);
    $messages[] = '<div>Вы не можете отправить форму не согласившись с контрактом.</div>';
  }

  $values = array();
  $sverh = array();
  $sverh['levit'] = "Левитация";
  $sverh['tp'] = "Телепортация";
  $sverh['walk'] = "Хождение сквозь стены";
  $sverh['vision'] = "Ночное зрение";
  $values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['year'] = empty($_COOKIE['birth_value']) ? '' : $_COOKIE['birth_value'];
  $values['sex'] = empty($_COOKIE['sex_value']) ? 'male' : $_COOKIE['sex_value'];
  $values['limbs'] = empty($_COOKIE['limbs_value']) ? '4' : $_COOKIE['limbs_value'];
  $values['bio'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];

  if (!empty($_COOKIE['sverh_value'])) {
      $sverh_value = json_decode($_COOKIE['sverh_value']);
  }
  $values['sverh'] = [];
  if (isset($sverh_value) && is_array($sverh_value)) {
      foreach ($sverh_value as $sverh) {
          if (!empty($sverh[$sverh])) {
              $values['sverh'][$sverh] = $sverh;
          }
      }
  }

  include('form.php');
}
else {

  $errors = FALSE;
  if (empty($_POST['name'])) {

    setcookie('name_error', 'null', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else if (!preg_match("#^[aA-zZ0-9-]+$#", $_POST["name"])) {
      setcookie('name_error', 'incorrect', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
  
    setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['email'])) {
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
  }

  $sverh = array();
  foreach ($_POST['sverh'] as $key => $value) {
      $sverh[$key] = $value;
  }
  if (!sizeof($sverh)) {
    setcookie('sverh_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('sverh_value', json_encode($sverh), time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['bio'])) {
    setcookie('bio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('bio_value', $_POST['bio'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['consent'])) {
    setcookie('consent_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }

  setcookie('birth_value', $_POST['birth'], time() + 30 * 24 * 60 * 60);
  setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
  setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);

// *************
// *************

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    
    setcookie('name_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('sverh_error', '', 100000);
    setcookie('bio_error', '', 100000);
    setcookie('consent_error', '', 100000);
   
  }

    // Параметры для подключения
    $db_user = "u17435"; // Логин БД
    $db_password = "1419686"; // Пароль БД
    $db_table = "application"; // Имя Таблицы БД

    $name = $_POST['name'];
    $email = $_POST['email'];
    $birth = $_POST['year'];
    $sex = $_POST['sex'];
    $limbs = $_POST['limbs'];
    $bio = $_POST['bio'];
    $consent = $_POST['consent'];
    $sverh_bd = array();
    foreach ($_POST['sverh'] as $key => $value) {
        $sverh_bd[$key] = $value;
    }
    $sverh_string = implode(', ', $sverh_bd);

    try {
        // Подключение к бд
        $db = new PDO('mysql:host=localhost;dbname=u17435', $db_user, $db_password, array(PDO::ATTR_PERSISTENT => true));

        // Создаем запрос
        $statement = $db->prepare("INSERT INTO ".$db_table." (name, email, birth, sex, limbs, sverh, bio) VALUES ('$name','$email',$birth,'$sex',$limbs,'$sverh_string','$bio')");

        $statement = $db->prepare('INSERT INTO '.$db_table.' (name, email, birth, sex, limbs, sverh, bio) VALUES (:name, :email, :birth, :sex, :limbs, :sverh, :bio)');

        $statement->execute([
            'name' => $name,
            'email' => $email,
            'birth' => $birth,
            'sex' => $sex,
            'limbs' => $limbs,
            'bio' => $bio,
            'sverh' => $sverh_string
        ]);
        setcookie('save', '1');
    } catch (PDOException $e) {
        setcookie('notsave', '1');
    }

  // Перенаправление.
  header('Location: index.php');
}