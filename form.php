<<html lang="ru">
  <head>
      <title>My Form</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link href="style.css" type="text/css" rel="stylesheet">
  </head>
  <body>

    <form action="" method="POST">

        

      <!-- Name -->
      <br>
      <label for="nameInput">Name </label>
      <input id="nameInput" name="name" type="text"  value="name" />
      <br>

      <!-- Email -->
      <br>
      <label for="emailInput">Email </label>
      <input id="emailInput" name="email" type="email"  value="example@gmail.com" />
      <br>

      <!-- Birth -->
      <br>
      <label for="selectInput">Birthday </label>
      <select name="birth">
          <option value="2014" >2014</option> <option value="2013" >2013</option> <option value="2012" selected >2012</option> <option value="2011" >2011</option> <option value="2010" >2010</option> <option value="2009" >2009</option> <option value="2008" >2008</option> <option value="2007" >2007</option> <option value="2006" >2006</option> <option value="2005" >2005</option> <option value="2004" >2004</option> <option value="2003" >2003</option> <option value="2002" >2002</option> <option value="2001" >2001</option> <option value="2000" >2000</option> <option value="1999" >1999</option> <option value="1998" >1998</option> <option value="1997" >1997</option> <option value="1996" >1996</option> <option value="1995" >1995</option> <option value="1994" >1994</option> <option value="1993" >1993</option> <option value="1992" >1992</option> <option value="1991" >1991</option> <option value="1990" >1990</option> <option value="1989" >1989</option> <option value="1988" >1988</option> <option value="1987" >1987</option> <option value="1986" >1986</option> <option value="1985" >1985</option> <option value="1984" >1984</option> <option value="1983" >1983</option> <option value="1982" >1982</option> <option value="1981" >1981</option> <option value="1980" >1980</option> <option value="1979" >1979</option> <option value="1978" >1978</option> <option value="1977" >1977</option> <option value="1976" >1976</option> <option value="1975" >1975</option> <option value="1974" >1974</option> <option value="1973" >1973</option> <option value="1972" >1972</option> <option value="1971" >1971</option> <option value="1970" >1970</option> <option value="1969" >1969</option> <option value="1968" >1968</option> <option value="1967" >1967</option> <option value="1966" >1966</option>       </select>
      <br>

      <!-- Sex -->
      <br>
      <label>Sex</label>
      <label>
          <input type="radio" name="sex" value="male" checked >
           Male
      </label>
      <label>
          <input type="radio" name="sex" value="female"  >
           Female
      </label>
      <br>

      <!-- Limbs -->
      <br>
      <label>Limbs count</label>
      <label>
          <input type="radio" name="limbs" value="2"  >
           2
      </label>
      <label>
          <input type="radio" name="limbs" value="4" checked >
           4
      </label>
      <label>
          <input type="radio" name="limbs" value="8"  >
           8
      </label>
      <br>

      <!-- Sverh -->
      <label for="sverhSelect">sverh</label>
      <select id="sverhSelect"  name="sverh[]" multiple size="4">
         <option value="levit", selected="selected" >Левитация</option><option value="tp",>Телепортация</option><option value="walk",>Хождение сквозь стены</option><option value="vision",>Ночное зрение</option>      </select>
      <br>

      <!-- Bio -->
      <br>
      <label for="bioArea">Bio</label>
      <textarea id="bioArea" name="bio" rows="8" cols="30" placeholder="Write somthing about you..." >1234</textarea>
      <br>

      <!-- Check -->
      <br>
      <label ><input type="checkbox" name="check" value="ok"> Accepted </label>
      <br>

      <!-- Button -->
      <br>
      <input type="submit" value="Send" />
    </form>
  </body>
</html>